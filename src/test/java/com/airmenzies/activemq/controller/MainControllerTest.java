package com.airmenzies.activemq.controller;

import com.airmenzies.activemq.ActivemqApplication;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

/**
 *
 * @author Craig Worsell
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ActivemqApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MainControllerTest {
    
    @Autowired
    private WebApplicationContext wac;
    
    private MockMvc mockMvc;
    private final ObjectMapper mapper = new ObjectMapper();
    
    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(wac)
                .build();
    }
    
    @Test
    public void formSubmissionTest() throws Exception {
        mockMvc.perform(post("/submitForm")
        .contentType(MediaType.MULTIPART_FORM_DATA));
        //.content(string));
    }
}
