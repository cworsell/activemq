package com.airmenzies.activemq.controller;

import com.airmenzies.activemq.form.JmsForm;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

/**
 *
 * @author Craig Worsell
 */
@Controller
public class MainController {
    
    @GetMapping("/home")
    public String getHome(Model model) {
        model.addAttribute("form", new JmsForm());
        return "index";
    }
    
    @PostMapping("/submitForm")
    public String postForm(JmsForm form) {
        ActiveMQConnectionFactory jmsConnection = new ActiveMQConnectionFactory();
        jmsConnection.setBrokerURL(form.getBrokerUrl());
        jmsConnection.setUserName(form.getUsername());
        jmsConnection.setPassword(form.getPassword());
        JmsTemplate template = new JmsTemplate();
        template.setConnectionFactory(jmsConnection);
        //template.setDefaultDestination(form.getDestination()));
        
        return "success";
    }
}
