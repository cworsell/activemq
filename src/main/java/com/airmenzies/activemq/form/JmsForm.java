package com.airmenzies.activemq.form;

import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Craig Worsell
 */
public class JmsForm {
    private String brokerUrl;
    private String username;
    private String password;
    private String destination;
    private boolean queue;
    private MultipartFile file;

    public JmsForm() {
    }

    public JmsForm(String brokerUrl, String username, String password, String destination, boolean queue) {
        this.brokerUrl = brokerUrl;
        this.username = username;
        this.password = password;
        this.destination = destination;
        this.queue = queue;
    }

    public String getBrokerUrl() {
        return brokerUrl;
    }

    public void setBrokerUrl(String brokerUrl) {
        this.brokerUrl = brokerUrl;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public boolean isQueue() {
        return queue;
    }

    public void setQueue(boolean queue) {
        this.queue = queue;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }
}
