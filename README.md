# ActiveMQ Consumer Spring Application

To run this application (and it's corresponding receiver application) you will need to do one of two things:

1. With a separate ActiveMQ instance:

    1. Download and install ActiveMQ as per the instructions on their site: [ActiveMQ](http://activemq.apache.org/download.html)
    2. Add the usernames and passwords to the `ActiveMQ\apache-activemq-<version>\conf\jetty-realm.properties` as per the application.yml files in each app
    3. Run ActiveMQ as per the instructions `activemq start' usually
    3. Start the apps the consumer first
2. With and internal ActiveMQ instance from Maven

    1. Comment out the following lines from the application.yml files in both apps:
    - ```spring:
        activemq:
            broker-url: tcp://localhost:61616
            user: appReceiver
            password: circl3.t4ble
            packages:
                trusted: org.springframework.remoting.support,java.lang,com.airmenzies.activemq```
    2. Add the config that is present in the Receiver app and the Receiver class to the Consumer app
    3. Run the Consumer app (this will now act as the consumer and the receiver)
